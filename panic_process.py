import subprocess

def erase_disk():
    DRIVE_ERASE = "/dev/sda2"

    # In MB
    # Default: 100
    # Note: first 100 MB is recommended in "a desperate hurry"
    # See Section 5.4 of https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#5-security-aspects
    SIZE_ERASE = 100

    ITERATION_ERASE = 7
    
    dd_count = str(int(SIZE_ERASE * 1024 * 1024 / 4096))

    for i in range(0, ITERATION_ERASE):
        dd_run = subprocess.run(["dd", "if=/dev/zero", "of="+DRIVE_ERASE, "bs=4096", "count="+dd_count], check=True, capture_output=True)
        print(dd_run)
        
    return True
