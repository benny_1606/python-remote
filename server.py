#!/usr/bin/env python3

import socketserver, hashlib, json, hmac, logging, datetime, re, subprocess
import config, panic_process
from base64 import standard_b64decode, standard_b64encode
from pathlib import Path
from enigma2.main import encryption_1606
from threading import Thread, Timer

logging.basicConfig(level=logging.DEBUG)

dir_path = Path(__file__).resolve().parent
hmac_list = list()

class MyTCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global hmac_list, dir_path
        epoch_now = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
        
        raw_data = self.request.recv(1025)
        # 1025 > 1024 to detect the malformed request
        raw_data_0_32 = raw_data[0:32]
        
        if len(raw_data) < 512 or len(raw_data) > 1024:
            logging.info(f"Malformed request received, len invalid, len={len(raw_data)}")
            return True
        
        raw_data_ctxt = raw_data[32:]
        raw_data_nonce = raw_data[32:56]
        raw_data_remainder = raw_data[56:]
        
        logging.info("{} connected".format(self.client_address[0]))
        logging.info(str(datetime.datetime.now(datetime.timezone.utc).isoformat()))
        logging.debug(f"raw_data_0_32 {raw_data_0_32[0:4]} len={len(raw_data_0_32)}")
        logging.debug(f"raw_data_nonce[0:2] {raw_data_nonce[0:2]} len={len(raw_data_nonce)}")
        logging.debug(f"raw_data_nonce[-2:] {raw_data_nonce[-2:]} len={len(raw_data_nonce)}")
        logging.debug(f"raw_data_remainder[0:2] {raw_data_remainder[0:2]} len={len(raw_data_remainder)}")
        logging.debug(f"raw_data_remainder[-2:] {raw_data_remainder[-2:]} len={len(raw_data_remainder)}")
        
        hmac_key = config.HMAC_KEY
        ctxt_hmac = hmac.new(hmac_key, raw_data_ctxt, hashlib.sha256)
        ctxt_hmac_hex = ctxt_hmac.hexdigest()
        ctxt_hmac_digest = ctxt_hmac.digest()
        logging.debug(f"ctxt_hmac_hex {ctxt_hmac_hex}")
        
        if ctxt_hmac_digest in hmac_list:
            logging.error("POSSIBLE REPLAY ATTACK")
            return True
        elif hmac.compare_digest(ctxt_hmac_digest, raw_data_0_32) != True:
            logging.error("INVALID HMAC")
            return True

        logging.debug("HMAC verified")
        
        enc_1606 = encryption_1606()                
        enc_1606.set_their_pk(standard_b64decode(config.client_pk))
        enc_1606.set_our_sk(standard_b64decode(config.our_sk))

        try:
            json_decrypt = enc_1606.decrypt_and_verify_sig(raw_data_ctxt)
            logging.debug("Decrypt Success")
        except ValueError as e:
            logging.error(f"Decrypt Error: {e}")
            return True
        
        try:
            if json_decrypt:
                json_decrypt = json_decrypt.decode().rstrip('0')
#                     logging.debug(f"json_decrypt {json_decrypt}")
#                     TBD: log info of json_decrypt in encrypted
                json_data = json.loads(json_decrypt)
                logging.debug('data_is_json')
        except json.JSONDecodeError as e:
            logging.error(f"json.JSONDecodeError: {e}")
            return True
        except ValueError as e:
            logging.error(f"Not json, caused by {e}")
            return True
        
        json_key_expected = config.JSON_KEY_EXPECTED
        json_key_checked = all (k in json_data for k in json_key_expected)
        
        if json_key_checked and epoch_now < json_data['t']:
            logging.error("Rejected \"future\" packet received, most likely caused by desync of time of server and/or client.")
            return True
        elif json_key_checked and epoch_now - json_data['t'] > config.PACKET_TIME_WINDOW:
            logging.error("POSSIBLE REPLAY ATTACK, rejected old packet")
            return True
        elif json_key_checked and epoch_now - json_data['t'] < config.PACKET_TIME_WINDOW:
            logging.info(f"Valid JSON received")
            hmac_list.append(ctxt_hmac_digest)
            # Do something here for the json
            if json_data['pp'] == True:
                
                desktop_alert_thread = Thread(target = self.run_desktop_alert)
                desktop_alert_thread.start()
                
                dd_timer = Timer(10, panic_process.erase_disk)
                dd_timer.start()
            
            
            return True
            
        elif json_key_checked == False:
            logging.error(f"expected: {json_key_expected}, got: {[*json_data]}")
            return True
        
    def run_desktop_alert(self):
        desktop_alert_path = str(dir_path / "desktop_alert.py")
        desktop_alert_run = subprocess.run(["/usr/bin/env", "DISPLAY=:0.0", "python3", desktop_alert_path], check=False, capture_output=True)

if __name__ == "__main__":
    logging.info(f"Listening on {config.PORT}")

    with socketserver.TCPServer((config.HOST, config.PORT), MyTCPHandler) as server:
        server.serve_forever()