PORT = 1111
HOST = "0.0.0.0"

# The public key of the client encoded in base64
client_pk = 'zcURZc3obDxO2VQZBwwzrVfbavaSLCJ6x2jyQZbCn2o='

# The secret key of the server encoded in base64
our_sk = 'VlcS1umApsazmfaWtK5HwzQIwI1BDrVScMWWD6MJpiI='

# The PSK of HMAC, in bytes
HMAC_KEY = b'w'

# Expected JSON keys
JSON_KEY_EXPECTED = ('t', '')

# Packet receievd later than this value (in seconds)
# will be rejected to prevent repeat attack.
# Packet time is determined by "t" in JSON.
# Default: 5
PACKET_TIME_WINDOW = 5