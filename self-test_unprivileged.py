from threading import Thread
from base64 import b85encode
import subprocess, pickle, sys, argparse, os

def demote():
    os.setgid(1000)
    os.setuid(1000)
    
    os.environ['XAUTHORITY'] = '/home/stalin/.Xauthority'
    os.environ['DISPLAY'] = ':0.0'
    os.environ['USER'] = 'stalin'
    os.environ['LOGNAME'] = 'stalin'
    os.environ['HOME'] = '/home/stalin'
    return True

def gpg_sign(challenge, response):
    proc = subprocess.Popen(['gpg', '--sign', '-u', 'B85F978249AD8727', '-'], universal_newlines=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    gpg_out, gpg_err = proc.communicate(input=challenge.encode())
    proc_return = proc.wait()
    if proc_return != 0:
        raise Exception(f"GPG exited with non-zero, return={proc_return}")
        sys.exit(1)
    
    response['g'] = b85encode(gpg_out).decode()
    return response['g']

def tpm2_totp(response):
    try:
        proc = subprocess.run(['/usr/local/bin/tpm2-totp', 'calculate'], check=True, capture_output=True)
    except:
        sys.exit(1)
    
    response['p'] = proc.stdout.decode()
    return response['p']

def main(challenge):
    response = dict()
    
    try:
        gpg_thread = Thread(target=gpg_sign, kwargs={'challenge':challenge, 'response':response})
        gpg_thread.start()
        
        tpm2_totp_thread = Thread(target=tpm2_totp, kwargs={'response':response})
        tpm2_totp_thread.start()
        
        gpg_thread.join()
        tpm2_totp_thread.join()
        
    except:
        raise Exception('Error occured. Aborting.')
        sys.exit(1)
        
    sys.stdout.buffer.write(pickle.dumps(response, protocol=pickle.HIGHEST_PROTOCOL))
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('challenge')
    args = parser.parse_args()
    try:
        demote()
    except:
        raise Exception('Cannot demote. This scripts need to run as root.')
        sys.exit(1)
        
    main(args.challenge)
